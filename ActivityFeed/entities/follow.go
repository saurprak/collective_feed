package entities

import (
	"github.com/gocql/gocql"
	"log"
)

func CreateFollow(follow Follow, session *gocql.Session) {

	if err := session.Query(`INSERT INTO collective.follow (id,Follow_Id,Follower_Id,Follow_Type,app_Id) VALUES ( ?, ?, ?, ?, ?)`,
		gocql.TimeUUID(), follow.FollowId, follow.FollowerId, follow.FollowType, follow.AppId).Exec(); err != nil {
		log.Fatal(err)
	}

}

func DeleteFollow(id string, session *gocql.Session) {
	if err := session.Query(` DELETE FROM collective.follow WHERE id =? `,
		id).Exec(); err != nil {
		log.Fatal(err)
	}
}

type Follow struct {
	FollowId   string
	FollowerId string
	FollowType string
	AppId      string
}
