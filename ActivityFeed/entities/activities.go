package entities

import (
	"github.com/gocql/gocql"
	"log"
)

type Job struct {
	Job     Activity
	Table   string
	Session *gocql.Session
}
var Jobs = make(chan Job, 100)
var Results = make(chan int, 100)

func CreateActivity(activity Activity, session *gocql.Session) {
	if err := session.Query(`INSERT INTO collective.activities (id,actor,verb,object,target,App_Id,foreign_id,hash_tags,custom_fields) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)`,
		gocql.TimeUUID(), activity.Actor, activity.Verb, activity.Object, activity.Target, activity.AppId, activity.ForeignId, activity.HashTags, activity.CustomFields).Exec(); err != nil {
		log.Fatal(err)
	}

	Jobs <-Job{activity,
	"session",
	session,
	}


}

func UpdateActivity(activity Activity, id string, session *gocql.Session) {
	if err := session.Query(` UPDATE collective.activities SET actor = ?,verb = ?,object = ?,target = ?,App_Id = ?,foreign_id = ?,hash_tags = ?,custom_fields = ? WHERE id =? `,
		activity.Actor, activity.Verb, activity.Object, activity.Target, activity.AppId, activity.ForeignId, activity.HashTags, activity.CustomFields, id).Exec(); err != nil {
		log.Fatal(err)
	}
}

func DeleteActivity(id string, session *gocql.Session) {
	if err := session.Query(` DELETE FROM collective.activities WHERE id =? `,
		id).Exec(); err != nil {
		log.Fatal(err)
	}
}

type Activity struct {
	Actor        string
	Verb         string
	Object       string
	Target       string
	ForeignId    string
	HashTags     string
	CustomFields string
	AppId        string
}
