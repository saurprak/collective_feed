package entities

import (
	"github.com/gocql/gocql"
	"log"
)

func CreateComments(comments Comments, session *gocql.Session) {

	if err := session.Query(`INSERT INTO collective.comments (id,Activity,Comment,parent_Id,User,app_id) VALUES ( ?, ?, ?, ?, ?, ?)`,
		gocql.TimeUUID(), comments.Activity, comments.Comment, comments.ParentId, comments.User, comments.AppId).Exec(); err != nil {
		log.Fatal(err)
	}
}

type Comments struct {
	Comment  string
	Activity string
	ParentId string
	User     string
	AppId    string
}
