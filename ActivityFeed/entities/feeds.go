package entities

import (
	"github.com/gocql/gocql"
	"log"
)

func CreateFeed(feed Feed, session *gocql.Session) {

	if err := session.Query(`INSERT INTO collective.feed (id,type,feed_group,producer,Activity,consumer,AppId) VALUES (?, ?, ?, ?, ?, ?, ?)`,
		gocql.TimeUUID(), feed.FeedType, feed.FeedGroup, feed.Producer, feed.Activity, feed.Consumer, feed.AppId).Exec(); err != nil {
		log.Fatal(err)
	}
}

type Feed struct {
	FeedType  string
	FeedGroup string
	Producer  string
	Activity  string
	Consumer  string
	AppId     string
}
