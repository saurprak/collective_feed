package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"io/ioutil"
	"encoding/json"
	"github.com/gocql/gocql"
	"github.com/bitly/go-simplejson"
	entities "./entities"
	worker "./worker"
)

var new_session = session()

func main() {
	new_session = session()
	r := mux.NewRouter()

	for w := 1; w <= 10; w++ {
		go worker.Worker(w, entities.Jobs, entities.Results)
	}
	r.HandleFunc("/activity", createActivityHandler).Methods("POST")
	r.HandleFunc("/activity/{id}", updateActivityHandler).Methods("PUT")
	r.HandleFunc("/activity/{id}", deleteActivityHandler).Methods("DELETE")
	r.HandleFunc("/feed", createFeedHandler).Methods("POST")
	r.HandleFunc("/follow", createFollowHandler).Methods("POST")
	r.HandleFunc("/follow", deleteFollowHandler).Methods("DELETE")
	r.HandleFunc("/comment", createCommentsHandler).Methods("POST")
	log.Fatal(http.ListenAndServe(":8000", r))
}
func deleteFollowHandler(writer http.ResponseWriter, request *http.Request) {
	params := mux.Vars(request)
	entities.DeleteFollow(params["id"], new_session)

	json := simplejson.New()
	json.Set("status", "success")
	payload, _ := json.MarshalJSON()
	writer.Header().Set("Content-Type", "application/json")
	writer.Write(payload)
}
func deleteActivityHandler(writer http.ResponseWriter, request *http.Request) {
	params := mux.Vars(request)
	entities.DeleteActivity(params["id"], new_session)

	json := simplejson.New()
	json.Set("status", "success")
	payload, _ := json.MarshalJSON()
	writer.Header().Set("Content-Type", "application/json")
	writer.Write(payload)
}

func session() (*gocql.Session) {
	cluster := gocql.NewCluster("52.32.56.122")
	cluster.Consistency = gocql.One
	cluster.Keyspace = "collective"
	cluster.Authenticator = gocql.PasswordAuthenticator{
		Username: "collective",
		Password: "##8090pure100##",
	}

	session, _ := cluster.CreateSession()
	return session
}

func createActivityHandler(writer http.ResponseWriter, request *http.Request) {

	body, err := ioutil.ReadAll(request.Body)
	if err != nil {
		panic(err)
	}
	var activity entities.Activity
	err = json.Unmarshal(body, &activity)

	fmt.Println(activity)

	if err != nil {
		panic(err)
	}

	entities.CreateActivity(activity, new_session)

	json := simplejson.New()
	json.Set("status", "success")
	payload, err := json.MarshalJSON()
	writer.Header().Set("Content-Type", "application/json")
	writer.Write(payload)
}

func updateActivityHandler(writer http.ResponseWriter, request *http.Request) {
	params := mux.Vars(request)
	fmt.Println(params)
	body, err := ioutil.ReadAll(request.Body)
	if err != nil {
		panic(err)
	}
	var activity entities.Activity
	err = json.Unmarshal(body, &activity)

	fmt.Println(activity)

	if err != nil {
		panic(err)
	}

	entities.UpdateActivity(activity, params["id"], new_session)

	json := simplejson.New()
	json.Set("status", "success")
	payload, err := json.MarshalJSON()
	writer.Header().Set("Content-Type", "application/json")
	writer.Write(payload)
}

func createFeedHandler(writer http.ResponseWriter, request *http.Request) {

	//defer new_session.Close()

	body, err := ioutil.ReadAll(request.Body)
	if err != nil {
		panic(err)
	}
	var feed entities.Feed
	err = json.Unmarshal(body, &feed)
	fmt.Println(feed)

	if err != nil {
		panic(err)
	}

	entities.CreateFeed(feed, new_session)

	json := simplejson.New()
	json.Set("status", "success")
	payload, err := json.MarshalJSON()
	writer.Header().Set("Content-Type", "application/json")
	writer.Write(payload)
}

func createFollowHandler(writer http.ResponseWriter, request *http.Request) {

	body, err := ioutil.ReadAll(request.Body)
	if err != nil {
		panic(err)
	}
	var follow entities.Follow
	err = json.Unmarshal(body, &follow)

	if err != nil {
		panic(err)
	}

	entities.CreateFollow(follow, new_session)

	json := simplejson.New()
	json.Set("status", "success")
	payload, err := json.MarshalJSON()
	writer.Header().Set("Content-Type", "application/json")
	writer.Write(payload)
}

func createCommentsHandler(writer http.ResponseWriter, request *http.Request) {

	body, err := ioutil.ReadAll(request.Body)
	if err != nil {
		panic(err)
	}
	var comments entities.Comments
	err = json.Unmarshal(body, &comments)

	if err != nil {
		panic(err)
	}

	entities.CreateComments(comments, new_session)

	json := simplejson.New()
	json.Set("status", "success")
	payload, err := json.MarshalJSON()
	writer.Header().Set("Content-Type", "application/json")
	writer.Write(payload)
}
