package worker

import (
	//"github.com/gocql/gocql"
	entities "../entities"
	//"log"
	//"fmt"
	//"encoding/json"

	"encoding/json"
	"github.com/gocql/gocql"
	"log"
)

func Worker(id int, Jobs <-chan entities.Job, results chan<- int) {
	/*for j := range Jobs {
		sum := 1
		for sum < 1000 {
			fmt.Println(j)
		}

	}*/
	var followerId string
	stmt := "INSERT INTO collective.feed (id,type,feed_group,producer,Activity,consumer,app_id) VALUES (?, ?, ?, ?, ?, ?, ?)"
	batch := gocql.NewBatch(gocql.LoggedBatch)
	count := 0;

	for j := range Jobs {

		count++;
		iter := j.Session.Query(`SELECT follower_id FROM collective.follow WHERE follow_id = ? ALLOW FILTERING`, j.Job.Actor).Iter()
		for iter.Scan(&followerId) {
			b, err := json.Marshal(j.Job)
			if err != nil {
				return
			}
			batch.Query(stmt, gocql.TimeUUID(), "type", "feed_group", j.Job.Actor, string(b), followerId, j.Job.AppId)
			if count%10 == 0 {
				err := j.Session.ExecuteBatch(batch)
				if err != nil {
					log.Panic(err)
				}
				batch = gocql.NewBatch(gocql.LoggedBatch)
			}
		}
		err := j.Session.ExecuteBatch(batch)
		if err != nil {
			log.Panic(err)
		}
		if err := iter.Close(); err != nil {
			log.Fatal(err)
		}
	}
}
