package main

import (
	"testing"
	entities "./entities"
	"github.com/gocql/gocql"
)

func TestFollower(t *testing.T) {
	cluster := gocql.NewCluster("52.32.56.122")
	cluster.Consistency = gocql.One
	cluster.Keyspace = "collective"
	cluster.Authenticator = gocql.PasswordAuthenticator{
		Username: "collective",
		Password: "##8090pure100##",
	}
	session, _ := cluster.CreateSession()
	follow := entities.Follow{
		"sam",
		"ursla",
		"testData",
		"testData",
	}



	entities.CreateFollow(follow, session)

}
/*

func TestActivity(t *testing.T){
	t.Run("follow", func(t *testing.T) {
		cluster := gocql.NewCluster("52.32.56.122")
		cluster.Consistency = gocql.One
		cluster.Keyspace = "collective"
		cluster.Authenticator = gocql.PasswordAuthenticator{
			Username: "collective",
			Password: "##8090pure100##",
		}
		session, _ := cluster.CreateSession()
		follow := entities.Follow{
			"sam",
			"ursla",
			"testData",
			"testData",
		}
		entities.CreateFollow(follow, session)

	})
	t.Run("activity", func(t *testing.T) {
		cluster := gocql.NewCluster("52.32.56.122")
		cluster.Consistency = gocql.One
		cluster.Keyspace = "collective"
		cluster.Authenticator = gocql.PasswordAuthenticator{
			Username: "collective",
			Password: "##8090pure100##",
		}
		session, _ := cluster.CreateSession()
		for w := 1; w <= 10; w++ {
			go worker.Worker(w, entities.Jobs, entities.Results)
		}

		activity := entities.Activity{"sam",
			"posted",
			"image_url",
			"@saurabh",
			"",
			"",
			"x", "123"}
		entities.CreateActivity(activity, session)

	})
}
/*func BenchmarkActivity(b *testing.B) {

	cluster := gocql.NewCluster("52.32.56.122")
	cluster.Consistency = gocql.One
	cluster.Keyspace = "collective"
	cluster.Authenticator = gocql.PasswordAuthenticator{
		Username: "collective",
		Password: "##8090pure100##",
	}
	session, _ := cluster.CreateSession()
	for w := 1; w <= 10; w++ {
		go worker.Worker(w, entities.Jobs, entities.Results)
	}
	*//*follow := entities.Follow{
		"sam",
		"ursla",
		"testData",
		"testData",
	}*//*
	activity := entities.Activity{"sam",
		"posted",
		"image_url",
		"@saurabh",
		"",
		"",
		"x", "123"}
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		entities.CreateActivity(activity, session)
	}

}*/

/*
func TestActivity(t *testing.T) {

	cluster := gocql.NewCluster("52.32.56.122")
	cluster.Consistency = gocql.One
	cluster.Keyspace = "collective"
	cluster.Authenticator = gocql.PasswordAuthenticator{
		Username: "collective",
		Password: "##8090pure100##",
	}
	session, _ := cluster.CreateSession()
	for w := 1; w <= 10; w++ {
		go worker.Worker(w, entities.Jobs, entities.Results)
	}

	activity := entities.Activity{"sam",
		"posted",
		"image_url",
		"@saurabh",
		"",
		"",
		"x", "123"}
	entities.CreateActivity(activity, session)


}
*/