# Fanout Design

We are going to use Push and Pull Fan-out approaches in phased manner.

## Phase #1 - Push Fan-out with write with no Ranking

In this phase, we will focus to build the fan-out worker framework and optmize for write operations.

### Simple Steps:

* Store the user activity into Activity Storage
* Publish the user activity into user feed first
* On Activity, get list of followers of a given user.
* Publish the user activity into feeds of its followers

### Detailed steps:

* Define follow_activity_limit and fanout_size as configurable variables. For example, you can have 5000 as follow_activity_limit and fanout_size is chunk size.  
* Define the priority ENUMs. For now two values low and high
* Create the fan-out tasks with follower id, activity, operation, feed_type, priority and other arguments. In the phase #1, keep the priority for each task as HIGH
* Push the fan-out task to queue or channel.  
* Worker pool run each task from queue. Each worker will update log and metrics table. Number of workers will be configurable.

### Technical design

There are following components in fan-out module

* Feed Collector - Feed collector accepts the activity and build the Tasks and push them into Feed Queue.
* Feed Queue - Feed Queue is pure implmenetation of distributed queue based on NSQ http://nsq.io/ using queue provider model
* Feed Dispatcher - Feed dispatcher is responsible for pulling Tasks from the Feed Queue and distribute them into available next worker.
* Worker -  Workers are responsible for performing a unit of work


## Phase 2

 * Fan-out using Ranking
 * Support for pull-based feed for followers

## Phase 3

* Support for notification feed
* Support for aggregated feed

## Phase 4

* Support for recommended feed
* Support for personalized feed

## Reference

http://nsq.io/overview/design.html
https://medium.com/@aalves/nsq-distributed-messaging-with-go-13b18140d639
https://github.com/kigster/simple-feed/ 
https://github.com/tschellenbach/Stream-Framework/blob/master/stream_framework/feed_managers/base.py 
