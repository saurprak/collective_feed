# Testing and Bench-marking

We are going to create test bed to run the performance and benchmarking of each operation using GoLang.

Need following test beds:

* Test the activitity with different payloads.
* Test the fan-out with Write for given activity with followers list
* Test the fan-out with Read  for given activity with followers list

## References
https://golang.org/pkg/testing/
